Version 1.0.9 / 2020-01-01

* [MAC] Support for macOS Catalina. User data moved to /Library/Audio/Presets/Mystery Islands Music/(plugin-name)/ folder. User must manually copy sound data from old /Documents/Mystery Islands Music/ folder to the new location.
* [New] Librarian Bank Edit Menu has a new item called "Open DATA folder" which will open the current plug-in data folder.


Version 1.0.8 / 2019-07-11

* [New] Program number display. User can now choose program from dropdown menu next to plug-in "display". If "load from library" is toggled on, user will see a list of banks and presets of banks in the menu.
* [New] "save data to slot" button in librarian allows user to quickly store data from plug-in UI to librarian. As a safety measure, user must then save the sound bank or discard changes.
* [New] Auto-save MIDI I/O. MIDI input and output are stored into text file when they are initially set. Next time the plug-in is inserted, these ports will be recalled and global data will be requested from the hardware.
* [Fixed] Resolved issue where Experimental MIDI Clock crashed Ableton Live.
* [Other] Some minor graphical updates, which you might not note...


Version 1.0.7 / 2019-05-22

* [Fixed] Resolved issue where patch sounds different after total recall. This bug is due to Pulse internal coding, but we found a work-around!


Version 1.0.6 / 2019-05-21

* [Fixed] Resolved issue where Single Banks, Category Files and Search items created multiple duplicates to menus when saving a new bank.
* [Fixed] Resolved issue which caused errors while storing bank with Save As.
* [Fixed] Resolved issue where search result entries where shown in lower bank selection menu after toggling off search function.
* [Fixed] Resolved issue where tab content did not update to new parameter values while clicking previous / next patch buttons.


Version 1.0.5 / 2019-05-14

* [Fixed] Resolved issue where graphics resize did not work as expected. Reload the project or plug-in in order the see skin and user interface size changes.


Version 1.0.4 / 2019-05-06

* [Other] Framework updates to make everything work smoother.
* [Other] Connections to hardware work better now when initializing the plug-in.


Version 1.0.3 / 2019-03-26

* [New] Section locks added and they are now fully functional.
* [Fixed] Resolved issue where plug-in crashed when clicking "randomize".
* [Fixed] Resolved issue where total recall did not work in some cases.


Version 1.0.2 / 2019-02-22

* [Fixed] Resolved issue where default program names were not applied to some presets when file was selected in librarian.


Version 1.0.1 / 2019-02-20

* [New] Importing Pulse 2 sounds has been improved. Now if slots 1-8 has destination set to Pitch, first match will be addressed to Pitch Mod slot in the plug-in. Same applies to Filter Mod, if destination is set to Cutoff. If these two matches are found from slots 1-4, they will be occupied from first used slot data from slots 5-8.
* [New] Right click menu has been reworked. User can now set category per patch by right click -> set category. User can now request current patch from plug-in interface to librarian. Keep in mind, that you still have to overwrite the bank or choose save as..
* [New] Librarian window now shows currently selected librarian patch category when clicking the item.
* [Fixed] Filter Mod Source and Filter Mod Amount values were sent and received the wrong way due to mismatch in original SysEx documentation.
* [Fixed] Resolved issue where Oscillator 1, 2 and 3 Semitones changed the entire patch in some cases.
* [Other] Reworked entire menu-system code. Now using ~720 lines of code instead ~14 000 lines.


Version 1.0.0 BETA / 2019-02-07

* Initial BETA release.
